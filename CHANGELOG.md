# Unreleased
## Added
## Changed
## Fixed

# [0.3.1] - 2024-06-06
## Added
- Fix rg-name

# [0.3.0] - 2024-05-30
## Added
- Moving nsg to saparate module 

# [0.2.1] - 2024-03-05
## Added
- Add `destination_address_prefixes` 

# [0.2.0] - 2024-03-04
## Added
- Add mandatory `location` and optional `security_rules` 

## Changed
- Remove `name_suffix`
