variable "name" {
  type = string
}

variable "location" {
  type = string
}

variable "vnet_name" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "address_prefixes" {
  type = list(string)
}

variable "service_endpoints" {
  type    = list(string)
  default = []
}

variable "delegation_name" {
  type    = string
  default = ""
}

variable "service_delegation_name" {
  type    = string
  default = ""
}
variable "service_delegation_actions" {
  type    = list(string)
  default = []
}
variable "security_rules" {
  type = map(object({
    priority                     = number
    access                       = optional(string, "Allow")
    source_address_prefixes      = list(string)
    source_port_range            = optional(string, "*")
    destination_address_prefixes = optional(list(string))
    destination_port_ranges      = list(string)
    direction                    = optional(string, "Inbound")
    protocol                     = optional(string, "Tcp")
  }))
  default = {}
}
