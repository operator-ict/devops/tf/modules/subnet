resource "azurerm_subnet" "subnet" {
  name                 = var.name
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.vnet_name
  address_prefixes     = var.address_prefixes
  service_endpoints    = var.service_endpoints

  dynamic "delegation" {
    for_each = var.delegation_name == null ? [] : ["X"]
    content {
      name = var.delegation_name
      service_delegation {
        name    = var.service_delegation_name
        actions = var.service_delegation_actions
      }
    }
  }
}

module "network-security-rules" {
  source   = "gitlab.com/operator-ict/network-security-rules/azurerm"
  version  = "0.1.0"

  name = "subnet-${var.name}"
  rg_name = var.resource_group_name
  rg_location = var.location
  default_destination_address_prefixes = var.address_prefixes
  security_rules = var.security_rules
}

resource "azurerm_subnet_network_security_group_association" "subnet" {
  count                     = length(var.security_rules) > 0 ? 1 : 0
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = module.network-security-rules.nsg_id
}
